package main

import "gitlab.com/notyetawizard/dsdr-server/dsdr"

func main() {
	s := dsdr.ServerInit()
	c := dsdr.ClientInit(s.Host)
	c.Write()
	s.Read()
	s.Write()
	c.Read()
}
