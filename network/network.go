// Package network can have a doc like this, or it's own file doc.go (Recommended)
package network

import(
	"net"
	"fmt"
)

func errMsg(err error) {
	if err != nil {
		fmt.Println("Error:", err)
	}
}

// Client is a type, which can listen to UDP datagrams from a Server
// and send UDP datagrams to a Server
type Client struct {
	user string
	conn *net.UDPConn
	addr *net.UDPAddr
}

// fix all client code!
func ClientInit(addr *net.UDPAddr) (*Client) {
	fmt.Println("Setting up client!")
	var c Client
	var err error
	c.addr = addr
	errMsg(err)
	
	c.conn, err = net.DialUDP("udp", nil, c.host)
	errMsg(err)
	
	return &c
}

func (c *Client) Read() {
	fmt.Println("Client reading ...")
	var err error
	buffer := make([]byte, 512)
	_, err = c.conn.Read(buffer)
	errMsg(err)
	
	fmt.Println(c.host, string(buffer))
}
	
func (c *Client) Write() {
	fmt.Println("Client writing ...")
	var err error
	_, err = c.conn.Write([]byte("Hello from client"))
	errMsg(err)
}

// Server is a type, which can listen to UDP datagrams from Clients
// and send UDP datagrams to Clients
type Server struct {
	conn *net.UDPConn
	addr *net.UDPAddr

	// Alter later to handle username or whatever!
	// clients should probably hold Client types, right?
	clients map[string]*net.UDPAddr
	MinClients int
	MaxClients int
	CurClients int
	TurnLength int //seconds
}

// NewServer sets up a new server, ready to listen.
// It takes an IP and port as a string ("127.0.0.1:7777") for it's first arg,
// and a number for max clients. Don't do to many; be reasonable, here.
func NewServer(addr string, maxClients int) Server {
	var s Server
	s.MaxClients = maxClients
	s.CurClients = 0
	s.clients = make(map[string]*net.UDPAddr, maxClients)
	var err error
	s.addr, err = net.ResolveUDPAddr("udp", addr)
	errMsg(err)
	s.conn, err = net.ListenUDP("udp", s.addr)
	errMsg(err)
	fmt.Println("Server listening at", s.addr)
	return s
}

// Run the fucking server
func (s *Server) Run() {
	
}

// Read listens for a datagram from a client, copying the payload into b
// This will block, so use a goroutine as needed!
// 
func (s *Server) Read(b []byte) (num int, err error) {
	// Need to, uh, do something with b if it's gonna be there.
	num, addr, err := s.conn.ReadFromUDP(b)
	errMsg(err)
	// Alter later to handle username or whatever!
	var user string
	// get username from message!
	// Check who sent the message
	for k, v := range s.clients {
		if k == user {
			// An existing client has sent a datagram
			// Need to confirm their ID, if it exists,
			// or set it if it doesn't
			if v == addr {
				// From their previous address
				return
			} else {
				// Reconnecting from a new address
				s.clients[k] = addr
				return
			}
		}
	}
	// Else a new client has sent a datagram
	if s.CurClients < s.MaxClients {
		// There is room to add them to the game!
		s.clients[user] = addr
		return
	} else {
		// Sorry, the game is full
		// Empty out b? Do something with it?
		fmt.Println("Connection from new client", user, "at", addr, "but the game is full.")
		return
	}
}

// fix server write!
func (s *Server) Write(b []byte) (num int, err error) {
	// Oh, it needs a way to say who to write to.
	// Time to break from Read/Write signitures and just do a better thing?
	// I think so.
	num, err = s.conn.WriteToUDP([]byte("Hello from server!"), s.addr)
	errMsg(err)
	return
}