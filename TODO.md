# TODO

## Up Next

[Dayna]
- ~~Tile struct and methods~~
- Sever side networking
- Terrain struct and methods
- Send terrain map to client
- Commands
- Mission control sensors
- Drone sensors
- Collision detection

[Devon]
- Very simple client (possibly manual, any language for now)
- Client side networking
- Terrain/Tile interpretation
- Receive terrain map from server
- Commands
- Mission Control sensor interpretation
- Drone sensor interpretation
- Collision prediction

## Later

- Simultaneous resolution rules
- Nice player and Drone IDs for datagrams
- Nice terrain generation

## Way Later

- Mass
- Payloads, money, trading
- Wrapping maps
- Planets in a system
- Drone hacking
- Drone types; equipment
- Structures and such