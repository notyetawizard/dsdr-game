package tiles

// Tile handling.
type Tile struct {
    data byte
    // LSB 0   bool Solid (Collision)
    // LSB 1   bool Entity (Collision)
    // LSB 2   bool Breakable (Interaction)
    // LSB 3   bool Moveable (Interaction)
    // LSB 4-7 0-15 Mass
    
    material byte
    // LSB 0-1 0-03 Strength
    // LSB 2-3 0-03 Energy
    // LSB 4-5 0-03 Conductivity
    // LSB 6-7 0-03 Beauty
}

// Check and set whether a tile is solid
func (t Tile) Solid() bool {
	if t.data & 0x01 != 0 {
		return true
	} else {
		return false
	}
}

func (t*Tile) SetSolid(v bool) {
	if v == true {
		t.data |= 0x01
	} else {
		t.data &^= 0x01
	}
}

// Check and set whether a tile has an entity
func (t Tile) Entity() bool {
	if t.data & 0x02 != 0 {
		return true
	} else {
		return false
	}
}

func (t*Tile) SetEntity(v bool) {
	if v == true {
		t.data |= 0x02
	} else {
		t.data &^= 0x02
	}
}

// Check if you would collide with a solid tile or entity
func (t Tile) Collision() bool {
	if t.data & 0x03 != 0 {
		return true
	} else {
		return false
	}
}

// Check and set whether a tile is breakable
func (t Tile) Break() bool {
	if t.data & 0x04 != 0 {
		return true
	} else {
		return false
	}
}

func (t*Tile) SetBreak(v bool) {
	if v == true {
		t.data |= 0x04
	} else {
		t.data &^= 0x04
	}
}

// Check and set whether a tile is moveable
func (t Tile) Move() bool {
	if t.data & 0x08 != 0 {
		return true
	} else {
		return false
	}
}

func (t*Tile) SetMove(v bool) {
	if v == true {
		t.data |= 0x08
	} else {
		t.data &^= 0x08
	}
}

// Check and set a tile's mass (does not include entity mass)
func (t Tile) Mass() byte {
	return t.data >> 4
}

// Note: if v > 15, mass will still be set to 15
func (t *Tile) SetMass(v byte) {
	if v >= 0x0F {
		t.data |= 0xF0
	} else {
		t.data = (t.data &^ 0xF0) | (v << 4)
	}
}

// Adds as much mass as it can to a tile before maxing, and returns the remainder
func (t *Tile) AddMass(v byte) byte {
	if v > (0x0F - (t.data >> 4)) {
		var r byte = v - (t.data >> 4)
		t.data |= 0xF0
		return  r
	} else {
		t.data += (v << 4)
		return 0x00
	}
}

// Removes as much mass as it can from a tile before zeroing, and returns the remainder
func (t *Tile) RemoveMass(v byte) byte {
	if v > (t.data >> 4) {
		var r byte = v - (t.data >> 4)
		t.data &^= 0xF0
		return r
	} else {
		t.data -= (v << 4)
		return 0x00
	}
}

// Check and set a tile's Strength value
func (t Tile) Strength() byte {
	return t.material &^ 0x03
}

// Note: if v >= 3, strength will be set to 3
func (t *Tile) SetStrength (v byte) {
	if v >= 0x03 {
		t.data |= 0x03
	} else {
		t.data = (t.data &^ 0x03) | v
	}
}

// Check a tile's Energy value
func (t Tile) Energy() byte {
	return t.material &^ 0x0C
}

// Note: if v >= 3, energy will be set to 3
func (t *Tile) SetEnergy (v byte) {
	if v >= 0x03 {
		t.data |= 0x0C
	} else {
		t.data = (t.data &^ 0x0C) | (v << 2)
	}
}

// Check a tile's Conductivity value
func (t Tile) Conductivity() byte {
	return t.material &^ 0x30
}

// Note: if v >= 3, conductivity will be set to 3
func (t *Tile) SetConductivity (v byte) {
	if v >= 0x03 {
		t.data |= 0x30
	} else {
		t.data = (t.data &^ 0x30) | (v << 4)
	}
}

// Check a tile's Energy value
func (t Tile) Beauty() byte {
	return t.material &^ 0xC0
}

// Note: if v >= 3, beauty will be set to 3
func (t *Tile) SetBeauty (v byte) {
	if v >= 0x03 {
		t.data |= 0xC0
	} else {
		t.data = (t.data &^ 0xC0) | (v << 6)
	}
}

// Check and set a tile's material ID
func (t Tile) Material() byte {
	return t.material
}

func (t *Tile) SetMaterial(v byte) {
	t.material = v
}


