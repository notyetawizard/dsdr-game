package tiles

import (
	"time"
	"math/rand"
	"fmt"
)

type Terrain struct {
	grid [][]Tile
	width int
	height int
}

func (t Terrain) hello() {
	for y := 0; y < t.height; y++ {
		for x := 0; x < t.width; x++ {
			fmt.Printf("%2.2X", t.grid[y][x].data)
		}
		print("\n")
	}
}

func NewTerrain(w int, h int) (Terrain) {
	var t Terrain
	t.grid = make([][]Tile, h)
	t.width = w
	t.height = h
	
	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	for y := 0; y < t.height; y++ {
		t.grid[y] = make([]Tile, t.width)
		for x := 0; x < t.width; x++ {
			if r.Intn(10) == 0 {
				t.grid[y][x].data = 0x20
			} else {
				t.grid[y][x].data = 0x00
			}
		}
	}
	
	return t
}