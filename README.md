# DSDR - Deep Space Drone Rigger

## Game Overview

##### Each player designs and deploys a payload of drones (one or more) to a planet. The game starts when the server has received all player's payloads.
- Normally the same planet, though competition on separate planets could be an interesting alternative game mode! Competitive colonization of an entire system?
- Each player starts with a fixed amount of money, and can outfit their drones and payload accordingly.
- The time it takes the payload to reach the planet is based on it's mass, and the distance to the planet.
- Starting strategy is about balancing cost, mass, and initial equipment effectively.

##### Once deployed, drones are controlled by the player via subspace signals. It is up to the player to design their own control systems (or use someone else's open source designs). Programming AI for the drones is going to be the most viable strategy due to the speed of the game and the advantages of leaving many drones unattended, though manual controls are an effective addition.
- Generally, have the drone explore a planet and discover things. Collect resources by mining and gathering. Drones may attack and steal from each other.
- If your drones are equipped for building, you can use resources directly for building things on the planet. Otherwise, you can sell your resources to passing trade ships for digital currency, delivered straight to mission control.
- Mission control can buy equipment and deploy new payloads to the planet at any time.

## Mechanisms of Play
	
##### The player sends UDP datagrams to the server each turn. One message may be sent for each drone in the game; if more than one datagram is received in a turn, only the newest is executed
- Each player is assigned a UUID and each drone is assigned a unique key by the server on creation, both of which are sent only to the controlling player's client.
- Each datagram is composed of (in order): the player's UUID, the drone key, and the action it should take.
- While strong enough to not make things easy, the drone keys are weak enough to be discovered by an opponent. This means that drones can be hacked by other players!
- As an action, a drone's key can be changed, and sent back to whichever player successfully requested the change.
- The player's UUID is actually strong and very unlikely to be cracked. 
	
##### Actions each turn are simultaneously resolved by wonderfully fair and predictable rules. Possible actions include:
- Moving one square in a cardinal direction.
- Mining, attacking, or gathering a nearby object.
- Building things
- Deploying a payload
	
##### The server sends some information to players every turn, and other information only occasionally.
- Information from drones' sensors is sent immediately each turn.
- Planetary scans are sent periodically, allowing a player to see things their drones cannot see themselves.
- Each player only gets to see what they should be able to see; information that should not be known is not sent.
